import React from "react";
import ReactDOM from "react-dom";
import WalkTheWorld from "./WalkTheWorld";

ReactDOM.render(<WalkTheWorld />, document.getElementById("root"))