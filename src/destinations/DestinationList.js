import React from "react";
import DestinationCard from "./DestinationCard";
import { Header, Card, Icon } from 'semantic-ui-react';

const DestinationList = (props) => {
    return (
        <React.Fragment>
            <Header as='h1' color='orange'>
                <Icon name='question circle' />
                <Header.Content>Where d'ya want to go?</Header.Content>
            </Header>
            <Card.Group itemsPerRow={3}>
            {props.Destinations.map((destination) => {
                return (
                    <DestinationCard key={destination.id} Destination={destination} />
                )
            })}
            </Card.Group>
        </React.Fragment>
    )
}

export default DestinationList;