import React from "react";
import DestinationCard from "./DestinationCard";
import { Header, Card, Icon } from 'semantic-ui-react';

const PopularDestinations = (props) => {

    return (
        <React.Fragment>
            <Header as="h2" color='orange'><Icon color='orange' name='gripfire' />Popular Destinations</Header>
            <Card.Group itemsPerRow={3}>
                {props.Destinations.sort(function (a, b) { return parseInt(a.visitors) < parseInt(b.visitors) ? 1 : -1 }).slice(0, 3).map((destination) => {
                    return <DestinationCard key={destination.id} Destination={destination} />
                })}
            </Card.Group>
        </React.Fragment>
    )
}

export default PopularDestinations;