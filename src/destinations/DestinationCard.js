import React from "react";
import { Card, Icon, Image, Button, Header } from 'semantic-ui-react';

const DestinationCard = (props) => {

    const { title, highlights, visitors, description, image } = props.Destination;
    return (
        <Card>
            <Image src={image} size='large' />
            <Card.Content>
                <Card.Header>
                    <Header as='h3' color='orange'>
                        <Icon name='map pin' />
                        <Header.Content>{title}</Header.Content>
                    </Header> 
                </Card.Header>
                <Card.Meta style={{ marginTop: '5px' }}>
                    <span>Highlights: {highlights.join(', ')}</span>
                </Card.Meta>
                <Card.Description >
                    {description}<br /><br />
                </Card.Description>
                <Button color='orange'>Book Now</Button>
            </Card.Content>
            <Card.Content extra>
                <Icon name='user' />
                {visitors} monthly visitors
            </Card.Content>
        </Card>
    )
}

export default DestinationCard;