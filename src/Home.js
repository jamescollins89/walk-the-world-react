import React from "react";
import { Grid, Divider } from 'semantic-ui-react';
import PopularDestinations from "./destinations/PopularDestinations";
import UpcomingFlights from "./flights/UpcomingFlights";
import Wishlist from "./wishlist/Wishlist";

const Home = (props) => {
    return (
        <React.Fragment>
            <Grid>
                <Grid.Row>
                    <Grid.Column>
                        <PopularDestinations Destinations={props.Destinations} />
                    </Grid.Column>
                </Grid.Row>
                <Divider />
                <Grid.Row>
                    <Grid.Column width={8}>
                        <UpcomingFlights Flights={props.Flights} />
                    </Grid.Column>
                    <Grid.Column width={8}>
                        <Wishlist />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </React.Fragment>
    )
}

export default Home;