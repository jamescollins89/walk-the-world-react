import React from "react";
import { Link } from "react-router-dom";
import { Grid, Menu, Button, Icon, Header } from 'semantic-ui-react';

const Navbar = () => {
    return (
        <React.Fragment>
            <Grid style={{marginBottom: '20px'}}>
                <Grid.Row verticalAlign='middle'>
                    <Grid.Column width={6}>
                        <Header color='blue' as='h1'>
                        <Icon  name='world' />
                        <Header.Content>Walk The World</Header.Content>
                        </Header>
                    </Grid.Column>
                    <Grid.Column  width={10}>
                        <Menu floated='right' compact>
                            <Menu.Item as={Link} to="/">Home</Menu.Item>
                            <Menu.Item as={Link} to="/destinations">Destinations</Menu.Item>
                            <Menu.Item as={Link} to="/flights">Flights</Menu.Item>
                            <Menu.Item as={Link} to="/booking-form">
                                <Button primary>Book Now</Button>
                            </Menu.Item>
                        </Menu>
                    </Grid.Column>
                </Grid.Row>
            </Grid>

        </React.Fragment>
    )
}

export default Navbar;