import React from "react";
import { Grid, Icon, Header } from 'semantic-ui-react';

const Wishlist = () => {
    return (
        <React.Fragment>
        <Header as='h2' color='yellow'><Icon color='yellow' name='star' />Wishlist</Header>
        <Grid celled verticalAlign='middle'>
            <Grid.Row>
                <Grid.Column width={6} verticalAlign='middle'>
                    <Icon color='yellow' name='map pin' />
                    <strong>New York</strong>
                </Grid.Column>
                <Grid.Column width={6}>
                    <Icon name='calendar' />
                    Spring 2021
                </Grid.Column>
                <Grid.Column width={4} textAlign='center'>
                    <Icon color='red' name='close' /><span style={{color: 'red'}}>Remove</span>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                <Grid.Column width={6} verticalAlign='middle'>
                    <Icon color='yellow' name='map pin' />
                    <strong>Tokyo</strong>
                </Grid.Column>
                <Grid.Column width={6}>
                    <Icon name='calendar' />
                    Summer 2022
                </Grid.Column>
                <Grid.Column width={4} textAlign='center'>
                    <Icon color='red' name='close' /><span style={{color: 'red'}}>Remove</span>
                </Grid.Column>
            </Grid.Row>
        </Grid>
        </React.Fragment>
    )
}

export default Wishlist;