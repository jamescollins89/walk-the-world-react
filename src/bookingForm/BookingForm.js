import React, { useState } from "react";
import { Grid, Icon, Header, Popup, Checkbox, Form, Button, Select } from 'semantic-ui-react';

const BookingForm = (props) => {

    const [flightToggle, setFlightToggle] = useState(false);
    const toggleFlightSelected = () => setFlightToggle(true);

    const [flightData, setFlightData] = useState({
        departure: '',
        arrival: '',
        departureDate: ''
    });

    const FLIGHTLIST = props.Flights.map((flight) => {
        return (
            { key: flight.flightNumber, value: flight.flightNumber, text: `${flight.departureDate} - ${flight.departure} > ${flight.arrival}` }
        )
    });

    let flightSelected = (e, { value }) => {
        toggleFlightSelected();

        let flight = props.Flights.filter(flight => flight.flightNumber.includes(value));
        let selectedFlight = flight[0];
        setFlightData({
            ...flightData,
            departure: selectedFlight.departure,
            arrival: selectedFlight.arrival,
            departureDate: selectedFlight.departureDate
        });
        console.log(flightData);

    }

    return (
        <React.Fragment>
            <Header as='h2' color='blue'>Book Today</Header>
            <Form>
                <Form.Group widths='equal'>
                    <Form.Input fluid label='First name' placeholder='First name' />
                    <Form.Input fluid label='Last name' placeholder='Last name' />
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.Input fluid label='Telephone Number' placeholder='Telephone Number' />
                    <Form.Input fluid label='Email Address' placeholder='Email Address' />
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.Select fluid label='Available Flights' placeholder='Available Flights' options={FLIGHTLIST} onChange={flightSelected} />
                </Form.Group>
                {flightToggle &&
                    <Form.Group widths='equal'>
                        <Form.Input fluid label='Flying From' placeholder='Flying From' value={flightData.departure} />
                        <Form.Input fluid label='Flying To' placeholder='Flying To' value={flightData.arrival} />
                        <Form.Input fluid label='Departure Date' placeholder='Departure Date' value={flightData.departureDate} />
                    </Form.Group>
                }
                <Form.Field>
                    <Checkbox label='I agree to the Terms and Conditions' />
                </Form.Field>
                <Button type='submit'>Submit</Button>
            </Form>
        </React.Fragment>
    )
}

export default BookingForm;