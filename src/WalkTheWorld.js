import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import { Container } from 'semantic-ui-react';
import Navbar from "./Navbar";
import Home from "./Home";
import Destinations from "./data/destinations";
import DestinationList from "./destinations/DestinationList";
import Flights from "./data/flightdata";
import FlightList from "./flights/FlightList";
import BookingForm from "./bookingForm/BookingForm";

const WalkTheWorld = () => {
    return (
        <Container style={{paddingTop: '20px', paddingBottom: '200px'}}>
            <Router>
                <Navbar />
                <Switch>
                    <Route exact path="/" render={() =>
                        <Home
                            Destinations={Destinations}
                            Flights={Flights}
                        />}
                    />
                    <Route path="/destinations" render={() =>
                        <DestinationList Destinations={Destinations} />
                    } />
                    <Route path="/flights" render={() =>
                        <FlightList Flights={Flights} />
                    } />
                    <Route path="/booking-form" render={() =>
                        <BookingForm Flights={Flights} />
                    } />
                </Switch>
            </Router>
        </Container>
    )
}

export default WalkTheWorld;