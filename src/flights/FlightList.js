import React from "react";
import { Card, Icon, Image, Button, Header } from 'semantic-ui-react';

const FlightList = (props) => {

    return (
        <React.Fragment>
            <Header as='h1' color='teal'>
                <Icon name='list' />
                <Header.Content>Flight List</Header.Content>
            </Header>
            <Card.Group itemsPerRow={4}>
                {props.Flights.map((flight) => {
                    return (
                        <Card>
                            <Image src="https://media.nomadicmatt.com/bookaflight.jpg" wrapped ui={false} />
                            <Card.Content>
                                <Card.Header>
                                    <Header as='h3' color='teal'>
                                        <Icon name='plane' />
                                        <Header.Content>{flight.arrival}</Header.Content>
                                    </Header>
                                </Card.Header>
                                <Card.Meta style={{ marginTop: '5px' }}>
                                    <span>Duration: {flight.duration} hours<br />
                                        {flight.departure} - {flight.arrival}
                                    </span>
                                </Card.Meta>
                                <Card.Description >
                                    <strong>{flight.departureDate}</strong><br /><br />
                                </Card.Description>
                                <Button color='teal'>Book Now</Button>
                            </Card.Content>
                            {parseInt(flight.stops) >= 1 ?
                                <Card.Content extra>
                                    <Icon name='warning' /> This flight has {flight.stops} stops
                        </Card.Content>
                                : null}
                        </Card>
                    );
                })}

            </Card.Group>
        </React.Fragment>
    )
}

export default FlightList;