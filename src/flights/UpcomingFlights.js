import React from "react";
import { Grid, Icon, Header, Popup } from 'semantic-ui-react';

const UpcomingFlights = (props) => {
    return (
        <React.Fragment>
            <Header as='h2' color='teal'><Icon color='teal' name='calendar' />Upcoming Flights</Header>
            {props.Flights.sort(function (a, b) { return parseInt(a.departureDate) > parseInt(b.departureDate) ? 1 : -1 }).slice(0, 3).map((flight) => {
                return (
                    <Grid celled key={flight.flightNumber} verticalAlign='middle'>
                        <Grid.Row verticalAlign='middle'>
                            <Grid.Column textAlign='center' width={6} verticalAlign='middle'>
                                <Grid.Row>
                                    <Icon name='plane' inverted circular color='teal' />
                                    </Grid.Row>
                                    <Grid.Row>
                                    <Header as='h5' style={{marginTop: '5px'}}>{flight.departureDate}</Header>
                                    </Grid.Row>
                            </Grid.Column>
                            <Grid.Column width={10}>
                                <Grid.Row>
                                <Icon name='map pin' />{flight.arrival} ({flight.flightNumber})
                        </Grid.Row>
                                <Grid.Row>
                                    {flight.departure} - <span style={{marginRight: '5px'}}>Duration: {flight.duration} hours</span>
                                    {parseInt(flight.stops) >= 1 ?
                                    <Popup content='This flight has multiple stops' trigger={<Icon circular inverted color='red' size='tiny' name='warning' />} />
                                    : null}
                                </Grid.Row>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                )
            })}
        </React.Fragment>
    )
}

export default UpcomingFlights;